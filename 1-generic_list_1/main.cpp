#include <iostream>

using namespace std;

void append(unsigned char* stash, int& pos, int x){
    // stash[pos] = x;

    unsigned char* temp = stash;
    temp += pos;

    *(int*)temp = x;

    pos += sizeof(int);

    // *(int*)temp = x;
}

void append(unsigned char* stash, int& pos, float x){
    unsigned char* temp = stash;
    temp += pos;

    *(float*)temp = x;

    pos += sizeof(float);
}

void append(unsigned char* stash, int& pos, char x){
    unsigned char* temp = stash;
    temp += pos;

    *(char*)temp = x;

    pos += sizeof(char);
}

void print(unsigned char* stash, int size, char types[]){
    unsigned char* temp = stash;
    for (int i = 0; i < size; i++){
        switch (types[i]) {
        case 'I':
            // if types[i] is I
            cout << *(int*)temp << " ";
            temp += sizeof(int);
            break;
        case 'C':
            cout << *(char*)temp << " ";
            temp += sizeof(char);
            break;

        case 'F':
            cout << *(float*)temp << " ";
            temp += sizeof(float);
            break;
        }
    }
    cout << endl;
}

int main(int argc, char* argv[]){
    // [5, 'A', 3.5]

    int pos = 0;
    unsigned char *p = new unsigned char[10]; // We have 10 consecutive memory cells

    char types[4];

    append(p, pos, 14);
    types[0] = 'I';
    append(p, pos, 'A');
    types[1] = 'C';
    append(p, pos, 3.14159f);
    types[2] = 'F';

    print(p, 3, types);

    delete[] p;

	
    return 0;
}
