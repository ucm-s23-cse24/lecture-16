#include <iostream>

using namespace std;

void append(void** p, int pos, int x){
    p[pos] = new int(x);
}

void append(void** p, int pos, float x){
    p[pos] = new float(x);
}

void append(void** p, int pos, char x){
    p[pos] = new char(x);
}


void print(void** p, int size, char types[]){
    for (int i = 0; i < size; i++){
        if (types[i] == 'C'){
            cout << *(char*)p[i] << " ";
        }
        else if (types[i] == 'I'){
            cout << *(int*)p[i] << " ";
        }
         else if (types[i] == 'F'){
            cout << *(float*)p[i] << " ";
        }
    }
    cout << endl;
}

int main(int argc, char* argv[]){

	// Your code here

    void** p = new void*[4];
    char types[4];

    for (int i = 0; i < 4; i++){
        p[i] = nullptr;
    }

    append(p, 0, 17);
    types[0] = 'I';

    append(p, 1, 'B');
    types[1] = 'C';

    append(p, 2, 1.5f);
    types[2] = 'F';

    print(p, 3, types);

    return 0;
}
